# THIS FILE IS GENERATED AUTOMATICALLY FROM THE D-I PO MASTER FILES
# The master files can be found under packages/po/
#
# DO NOT MODIFY THIS FILE DIRECTLY: SUCH CHANGES WILL BE LOST
#
# Catalan messages for debian-installer.
# Copyright 2002-2008, 2010, 2012, 2015, 2017-2018 Software in the Public Interest, Inc.
# This file is distributed under the same license as debian-installer.
# Jordi Mallach <jordi@debian.org>, 2002-2008, 2010, 2012, 2015, 2017-2018.
# Guillem Jover <guillem@debian.org>, 2005, 2007.
# d <dmanye@gmail.com>, 2021.
#
# Translations from iso-codes:
# Alastair McKinstry <mckinstry@computer.org>, 2001.
# Free Software Foundation, Inc., 2002,2004,2006
# Orestes Mas i Casals <orestes@tsc.upc.es>, 2004-2006. (orestes: He usat la nomenclatura de http://www.traduim.com/)
# Softcatalà <info@softcatala.org>, 2000-2001
# Toni Hermoso Pulido <toniher@softcatala.cat>, 2010.
# Traductor: Jordi Ferré <jordiferre@catalonia.altranet.fr>
msgid ""
msgstr ""
"Project-Id-Version: debian-installer buster\n"
"Report-Msgid-Bugs-To: cdrom-checker@packages.debian.org\n"
"POT-Creation-Date: 2019-09-19 16:20+0000\n"
"PO-Revision-Date: 2021-02-12 18:39+0000\n"
"Last-Translator: d <dmanye@gmail.com>\n"
"Language-Team: Catalan <debian-l10n-catalan@lists.debian.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#. Type: boolean
#. Description
#. :sl3:
#: ../cdrom-checker.templates:1001
msgid "Check integrity of the installation media?"
msgstr "Verificar la integritat dels mitjà d'instal·lació?"

#. Type: boolean
#. Description
#. :sl3:
#: ../cdrom-checker.templates:1001
msgid "Warning: this check depends on your hardware and may take some time."
msgstr ""
"Avís: aquesta comprovació depèn del vostre maquinari i pot trigar una estona."

#. Type: note
#. Description
#. :sl3:
#: ../cdrom-checker.templates:2001
msgid "Insert Debian installation media"
msgstr "Inseriu el mitjà d'instal·lació de Debian"

#. Type: note
#. Description
#. :sl3:
#: ../cdrom-checker.templates:2001
msgid ""
"Please insert one of the official Debian installation media before "
"continuing."
msgstr ""
"Inseriu un dels mitjans d'instal·lació oficials de Debian abans de continuar."

#. Type: error
#. Description
#. :sl3:
#: ../cdrom-checker.templates:3001
msgid "Failed to mount installation media"
msgstr "No s'ha pogut muntar el mitjà d'instal·lació"

#. Type: error
#. Description
#. :sl3:
#: ../cdrom-checker.templates:3001
msgid ""
"The device '${CDROM}' could not be mounted correctly. Please check the media "
"and cables, and try it again."
msgstr ""
"El dispositiu '${CDROM}' no s'ha pogut muntar correctament. Si us plau, "
"comproveu el medi i els cables, i proveu de nou."

#. Type: error
#. Description
#. :sl3:
#: ../cdrom-checker.templates:4001
msgid "No valid Debian installation media"
msgstr "Mitjà d'instal·lació de Debian invàlid"

#. Type: error
#. Description
#. :sl3:
#: ../cdrom-checker.templates:4001
msgid ""
"What you have inserted is no valid Debian installation media. Please try "
"again."
msgstr ""
"El que heu inserit no és un mitjà d'instal·lació de Debian vàlid. Si us "
"plau, canvieu el disc."

#. Type: error
#. Description
#. TRANSLATORS: MD5 is a file name, don't translate it.
#. :sl3:
#: ../cdrom-checker.templates:5001
msgid "Failed to open checksum file"
msgstr "No s'ha pogut obrir el fitxer de sumes de comprovació"

#. Type: error
#. Description
#. TRANSLATORS: MD5 is a file name, don't translate it.
#. :sl3:
#: ../cdrom-checker.templates:5001
msgid ""
"Opening the MD5 file on the media failed. This file contains the checksums "
"of the files located on the media."
msgstr ""
"No s'ha pogut obrir el fitxer d'MD5 del mitjà. Aquest fitxer conté les sumes "
"de comprovació dels fitxers ubicats al mitjà."

#. Type: note
#. Description
#. :sl3:
#: ../cdrom-checker.templates:6001
msgid "Integrity test successful"
msgstr "Comprovació d'integritat correcta"

#. Type: note
#. Description
#. :sl3:
#: ../cdrom-checker.templates:6001
msgid "The integrity test was successful. The installation image is valid."
msgstr ""
"La comprovació d'integritat ha estat correcta. La imatge d'instal·lació és "
"vàlida."

#. Type: error
#. Description
#. :sl3:
#: ../cdrom-checker.templates:7001
msgid "Integrity test failed"
msgstr "Ha fallat la comprovació d'integritat"

#. Type: error
#. Description
#. :sl3:
#: ../cdrom-checker.templates:7001
msgid ""
"The file ${FILE} failed the MD5 checksum verification. Your installation "
"media or this file may have been corrupted."
msgstr ""
"El fitxer ${FILE} ha fallat la verificació de les sumes de comprovació MD5. "
"El vostre mitjà d'instal·lació o aquest fitxer poden estar corromputs."

#. Type: boolean
#. Description
#. :sl3:
#: ../cdrom-checker.templates:8001
msgid "Check the integrity of another installation image?"
msgstr "Voleu comprovar la integritat d'una altra imatge d'instal·lació?"

#. Type: note
#. Description
#. :sl3:
#: ../cdrom-checker.templates:9001
msgid "Insert Debian boot media"
msgstr "Inseriu un mitjà d'arrencada de Debian"

#. Type: note
#. Description
#. :sl3:
#: ../cdrom-checker.templates:9001
msgid ""
"Please make sure you have inserted the Debian installation media you booted "
"from, to continue with the installation."
msgstr ""
"Assegureu-vos que heu inserit el mitjà d'instal·lació de Debian del qual heu "
"arrencat per a continuar amb la instal·lació."

#. Type: text
#. Description
#. :sl3:
#: ../cdrom-checker.templates:10001
msgid "Checking integrity of installation image"
msgstr "S'està verificant la integritat de la imatge d'instal·lació"

#. Type: text
#. Description
#. :sl3:
#: ../cdrom-checker.templates:11001
msgid "Checking file: ${FILE}"
msgstr "S'està comprovant el fitxer: ${FILE}"

#. Type: text
#. Description
#. Main menu item
#. Translators: keep it under 65 columns
#. :sl2:
#: ../cdrom-checker.templates:12001
msgid "Check the integrity of installation media"
msgstr "Comprovar la integritat dels mitjans d'instal·lació"
